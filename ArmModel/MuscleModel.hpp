#pragma once

#include <map>
#include <string>

#include "srDyn/srSpace.h"			// to access sr-elements

using namespace std;
class MuscleModel
{
public:
	MuscleModel()
		:_name(""), _parentLink(NULL), _childLink(NULL), _parentPoint(Vec3(0.0)), _childPoint(Vec3(0.0)),_L0(0.0), _F0(0.0)
	{
		//	do nothing
	}
	MuscleModel(const string& name, srLink* parentLinkPtr, srLink* childLinkPtr, const Vec3& parentLinkPoint, const Vec3& childLinkPoint)
		: _name(name), _parentLink(parentLinkPtr), _childLink(childLinkPtr), _parentPoint(parentLinkPoint), _childPoint(childLinkPoint), _L0(0.0), _F0(0.0)
	{
		//	do nothing
	}

	//	get absolute position
	const Vec3 getPositionParent() const
	{
		return  _parentLink->GetFrame() * _parentPoint;
	}

	const SE3 getSE3parent() const
	{
		return _parentLink->GetFrame() * SE3(_parentPoint);
	}

	const Vec3	getPositionChild() const
	{
		return _childLink->GetFrame() * _childPoint;
	}

	const SE3	getSE3child() const
	{
		return _childLink->GetFrame() * SE3(_childPoint);
	}

	double getLength() const
	{
		return Norm(getPositionChild() - getPositionParent());
	}

	double getTension() const
	{
		//	TODO : this is temporary implementation.
		return _F0 * getLength() / _L0;
	}

	//	variables
	string		_name;

	srLink*		_parentLink;
	srLink*		_childLink;

	Vec3		_parentPoint;
	Vec3		_childPoint;

	double _L0, _F0;
};