#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/Eigenvalues>
#include "srDyn/srSpace.h"			// for dynamics
//#include "common/utils.h"
#include "common/srExtension.h"
#include "MuscleModel.hpp"

//#define MAKE_MID_ARM_LOCK			//	6 DOF mode	
//#define MAKE_WRIST_YAW_ZERO			//	wrist_yaw is always zero, but actually model is 7 DOF.

#ifdef MAKE_MID_ARM_LOCK
#undef MAKE_WRIST_YAW_ZERO
#endif // MAKE_MID_ARM_LOCK

using namespace std;
using namespace Eigen;

class ArmModel : public srSystem
{
public:
	ArmModel();

	void setStateDeg(double shoulder_roll, double shoulder_pitch, double shoulder_yaw, double elbow_pitch, double elbow_roll, double wrist_pitch, double wrist_yaw);
	void setStateDeg(const VectorXd& _conf);
	void setStatePos(double shoulder_x, double shoulder_y, double shoulder_z, double elbow_x, double elbow_y, double elbow_z, double wrist_x, double wrist_y, double wrist_z);

	VectorXd	getStateDeg();
	//	caution : use only with 'hybrid mode' in srLib
	VectorXd	getTorque();

	void updateTension();

	void assembleModel();

	void attachMuscles();

	//	calculate counter force which would eliminates most of tendon trived torque. ('_counterForceFrame' sets frame of counter force, default is spatial frame)

	pair<Vector6d, VectorXd>	calcCounterForce(const SE3& _counterForceFrame = SE3());

	//protected:

		//	links
	srLink _link_base;
	srLink _link_humerus;
	srLink _link_ulna;
	srLink _link_radius;
	srLink _link_hand;
	map<string, srLink*>			_linkPtrs;

	// joints
	srBallJoint			_joint_shoulder;
	srRevoluteJoint		_joint_elbow;
#ifndef MAKE_MID_ARM_LOCK
	srRevoluteJoint		_joint_midArm;
#else
	srWeldJoint		_joint_midArm;
#endif
	srUniversalJoint	_joint_wrist;

	//	muscles
	map<string, MuscleModel>		_muscles;
};
