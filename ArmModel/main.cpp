#include "Renderer/SimpleViewer.h"	// for rendering
#include "srg/srgGeometryDraw.h"	// for User rendering
#include "srDyn/srSpace.h"			// for dynamics
#include "ArmModel.h"


// Get srSimpleViewer instance.
srSimpleViewer& gViewer = srSimpleViewer::GetInstance();	// Simple Viewer (singleton)
srSpace gSpace; // Space that systems will be included.
ArmModel myArm;

//////////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////////
void User_SimulationSetting();
void User_CBFunc_Render(void* pvData);

const double jointStep = 1.0;	//deg
VectorXd state = VectorXd::Zero(7);

void CBFunc_joint00_f()
{
	state[0] += jointStep;
}

void CBFunc_joint00_b()
{
	state[0] -= jointStep;
}

void CBFunc_joint01_f()
{
	state[1] += jointStep;
}

void CBFunc_joint01_b()
{
	state[1] -= jointStep;
}

void CBFunc_joint02_f()
{
	state[2] += jointStep;
}

void CBFunc_joint02_b()
{
	state[2] -= jointStep;
}

void CBFunc_joint03_f()
{
	state[3] += jointStep;
}

void CBFunc_joint03_b()
{
	state[3] -= jointStep;
}

void CBFunc_joint04_f()
{
	state[4] += jointStep;
}

void CBFunc_joint04_b()
{
	state[4] -= jointStep;
}

void CBFunc_joint05_f()
{
	state[5] += jointStep;
}

void CBFunc_joint05_b()
{
	state[5] -= jointStep;
}

void CBFunc_joint06_f()
{
	state[6] += jointStep;
}

void CBFunc_joint06_b()
{
	state[6] -= jointStep;
}

void CBFunc_reset()
{
	for (auto i = 0; i < 7; i++)
		state[i] = 0.0;
}


void	debugFunc()
{
	system("cls");
	//	body Jacobian vs spatial Jacobian
	//auto Js = srUtils::Jacobian(&myArm._link_hand, false);
	//auto Jb = srUtils::Jacobian(&myArm._link_hand, true);
	//MatrixXd err = Js - srUtils::AdMat(myArm._link_hand.GetFrame()) * Jb;
	//cout << "Jacobian max error : " << err.lpNorm<Infinity>() << endl;

	//	counter force
	myArm.updateTension();
	cout << "counter force in spatial frame : " << endl << myArm.calcCounterForce().first.transpose() << endl << endl;
	cout << "counter force in end effector frame : " << endl << myArm.calcCounterForce(myArm._link_hand.GetFrame()).first.transpose() << endl << endl;

	//	couter torque : calculation in model
	cout << "counter torque(Jacobian) : " << endl << myArm.calcCounterForce().second.transpose() << endl << endl;
	//	couter torque : srLib inverse dynamics results
	gSpace._DYN_Step_Forward_All_The_Systems();
	cout << "counter torque(srLib inv dyn) : " << endl << myArm.getTorque().transpose() << endl;

	//	debug inverse kinematics routine
	state = srExt::invKin(&myArm._link_hand, SE3(Vec3(0.5, 0, 0)), NULL) * SR_DEGREE;
	cout << "End-effector position : " << endl << myArm._link_hand.GetFrame().GetPosition() << endl;
}

int main(int argc, char **argv)
{
	gViewer.Init(&argc, argv);
	gViewer.SetKeyFunc(CBFunc_joint00_f, 'q');
	gViewer.SetKeyFunc(CBFunc_joint00_f, 'Q');
	gViewer.SetKeyFunc(CBFunc_joint00_b, 'a');
	gViewer.SetKeyFunc(CBFunc_joint00_b, 'A');

	gViewer.SetKeyFunc(CBFunc_joint01_f, 'w');
	gViewer.SetKeyFunc(CBFunc_joint01_f, 'W');
	gViewer.SetKeyFunc(CBFunc_joint01_b, 's');
	gViewer.SetKeyFunc(CBFunc_joint01_b, 'S');

	gViewer.SetKeyFunc(CBFunc_joint02_f, 'e');
	gViewer.SetKeyFunc(CBFunc_joint02_f, 'E');
	gViewer.SetKeyFunc(CBFunc_joint02_b, 'd');
	gViewer.SetKeyFunc(CBFunc_joint02_b, 'D');

	gViewer.SetKeyFunc(CBFunc_joint03_f, 'r');
	gViewer.SetKeyFunc(CBFunc_joint03_f, 'R');
	gViewer.SetKeyFunc(CBFunc_joint03_b, 'f');
	gViewer.SetKeyFunc(CBFunc_joint03_b, 'F');

	gViewer.SetKeyFunc(CBFunc_joint04_f, 't');
	gViewer.SetKeyFunc(CBFunc_joint04_f, 'T');
	gViewer.SetKeyFunc(CBFunc_joint04_b, 'g');
	gViewer.SetKeyFunc(CBFunc_joint04_b, 'G');

	gViewer.SetKeyFunc(CBFunc_joint05_f, 'y');
	gViewer.SetKeyFunc(CBFunc_joint05_f, 'Y');
	gViewer.SetKeyFunc(CBFunc_joint05_b, 'h');
	gViewer.SetKeyFunc(CBFunc_joint05_b, 'H');

	gViewer.SetKeyFunc(CBFunc_joint06_f, 'u');
	gViewer.SetKeyFunc(CBFunc_joint06_f, 'U');
	gViewer.SetKeyFunc(CBFunc_joint06_b, 'j');
	gViewer.SetKeyFunc(CBFunc_joint06_b, 'J');

	gViewer.SetKeyFunc(debugFunc, 'd');
	gViewer.SetKeyFunc(debugFunc, 'D');

	gSpace.AddSystem(&myArm);

	User_SimulationSetting();

	gViewer.Run();

	return 0;
}


//
void User_SimulationSetting()
{
	// Initialize for dynamics simulation.
	gSpace.DYN_MODE_PRESTEP();
	// Set target space to render.
	// Let srSimpleRenderer know what you want to draw on screen.
	gViewer.SetTarget(&gSpace);

	// Additional step: Set your user-render function.
	gViewer.SetUserRenderFunc(User_CBFunc_Render, NULL);

	gSpace.SetNumberofSubstepForRendering(1);
	gSpace.SetGravity(0.0, 0.0, 0.0);
	myArm.setStatePos(0, 0, 0, 10, 5, -7, 20, 3, -5);
	auto curr_state = myArm.getStateDeg();
#ifdef MAKE_MID_ARM_LOCK
	state << curr_state[0], curr_state[1], curr_state[2], curr_state[3], 0, curr_state[4], curr_state[5];
#else
	state = curr_state;
#endif
}



void User_CBFunc_Render(void* pvData)
{
	// you can draw whatever you want in OpenGL world
	srgMaterialColor color(1, 0, 0);
	color.PushAttrib();
	static Vec3 parent, child;

	//	순서대로 / shoulder의 roll, pitch, yaw / elbow의 pitch / elbow와 wrist의 중간 joint의 roll / wrist의 pitch, yaw
	myArm.setStateDeg(state[0], state[1], state[2], state[3], state[4], state[5], state[6]);

	for (auto iter = myArm._muscles.begin(); iter != myArm._muscles.end(); iter++)
	{
		parent = iter->second.getPositionParent();
		child = iter->second.getPositionChild();
		srgDrawLine(parent[0], parent[1], parent[2], child[0], child[1], child[2]);
	}
	color.PopAttrib();


	//color = srgMaterialColor(0, 0, 1);
	//color.PushAttrib();
	//parent = myArm._muscles["18"].getPositionParent();
	//child = myArm._muscles["18"].getPositionChild();
	//srgDrawLine(parent[0], parent[1], parent[2], child[0], child[1], child[2]);
	//color.PopAttrib();
}