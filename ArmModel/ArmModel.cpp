#include "ArmModel.h"

ArmModel::ArmModel()
{
	_linkPtrs.insert(make_pair("rest of the body", &_link_base));
	_linkPtrs.insert(make_pair("humerus", &_link_humerus));
	_linkPtrs.insert(make_pair("ulna", &_link_ulna));
	_linkPtrs.insert(make_pair("radius", &_link_radius));
	_linkPtrs.insert(make_pair("hand", &_link_hand));

	assembleModel();
	attachMuscles();
}

void ArmModel::setStateDeg(double shoulder_roll, double shoulder_pitch, double shoulder_yaw, double elbow_pitch, double elbow_roll, double wrist_pitch, double wrist_yaw)
{
	_joint_shoulder.GetBallJointState().m_SO3Pos = EulerZYX(Vec3(DEG2RAD(shoulder_yaw), DEG2RAD(shoulder_pitch), DEG2RAD(shoulder_roll))).GetOrientation();
	_joint_elbow.GetRevoluteJointState().m_rValue[0] = DEG2RAD(elbow_pitch);
#ifndef MAKE_MID_ARM_LOCK
	_joint_midArm.GetRevoluteJointState().m_rValue[0] = DEG2RAD(elbow_roll);
#endif // !MAKE_MID_ARM_LOCK
	_joint_wrist.GetUniversalJointState().m_rValue[0][0] = DEG2RAD(wrist_pitch);
#ifndef MAKE_WRIST_YAW_ZERO
	_joint_wrist.GetUniversalJointState().m_rValue[1][0] = DEG2RAD(wrist_yaw);
#endif
	KIN_UpdateFrame_All_The_Entity();
}

void ArmModel::setStateDeg(const VectorXd & _conf)
{
#ifdef MAKE_MID_ARM_LOCK
	setStateDeg(_conf[0], _conf[1], _conf[2], _conf[3], 0, _conf[4], _conf[5]);
#else
	setStateDeg(_conf[0], _conf[1], _conf[2], _conf[3], _conf[4], _conf[5], _conf[6]);
#endif
}

void ArmModel::setStatePos(double shoulder_x, double shoulder_y, double shoulder_z, double elbow_x, double elbow_y, double elbow_z, double wrist_x, double wrist_y, double wrist_z)
{
	//	initialize to zero position
	setStateDeg(0, 0, 0, 0, 0, 0, 0);
	double elbow_pitch, elbow_roll, wrist_pitch;

	Vector3d v0(shoulder_x, shoulder_y, shoulder_z);
	Vector3d v1(elbow_x, elbow_y, elbow_z);
	Vector3d v2(wrist_x, wrist_y, wrist_z);
	Vector3d Z(0.0, 0.0, 1.0);

	v2 = v2 - v1;
	v1 = v1 - v0;

	//	elbow pitch
	double l1 = v1.norm();
	double l2 = v2.norm();
	double l21 = (v2 - v1).norm();
	double cos_theta = (l1*l1 + l2*l2 - l21*l21) / (2 * l1*l2);
	elbow_pitch = -RAD2DEG(acos(cos_theta));

	//	elbow SO3
	Vector3d elbow_x_axis = v1.normalized();
	Vector3d elbow_z_axis = v2.cross(v1).normalized();
	Vector3d elbow_y_axis = elbow_z_axis.cross(elbow_x_axis).normalized();
	SO3	elbow_SO3(elbow_x_axis[0], elbow_x_axis[1], elbow_x_axis[2], elbow_y_axis[0], elbow_y_axis[1], elbow_y_axis[2], elbow_z_axis[0], elbow_z_axis[1], elbow_z_axis[2]);
	elbow_SO3 = elbow_SO3 * Inv(_joint_elbow.GetFrame().GetOrientation());
	Vec3 shoulder_ypr = iEulerZYX(elbow_SO3);

	//	elbow_roll
	Vector3d groundParallel = Z.cross(v2).normalized();
	elbow_roll = RAD2DEG(acos(elbow_z_axis.dot(groundParallel)));
	if (elbow_z_axis[2] > 0)
		elbow_roll *= -1;

	//	wrist_pitch
	Vector3d v2_ground_proj = v2;
	v2_ground_proj[2] = 0;
	cos_theta = v2_ground_proj.norm() / l2;
	wrist_pitch = RAD2DEG(acos(cos_theta));
	if (v2[2] < 0)
		wrist_pitch *= -1;

	setStateDeg(RAD2DEG(shoulder_ypr[2]), RAD2DEG(shoulder_ypr[1]), RAD2DEG(shoulder_ypr[0]), elbow_pitch, elbow_roll, wrist_pitch, 0.0);
}

VectorXd ArmModel::getStateDeg()
{
	int idx = 0;
	VectorXd q(srExt::count_dof(&_link_hand));
	auto ypr = iEulerZYX(_joint_shoulder.GetBallJointState().m_SO3Pos);
	q(idx++) = ypr[2] * SR_DEGREE;
	q(idx++) = ypr[1] * SR_DEGREE;
	q(idx++) = ypr[0] * SR_DEGREE;
	q(idx++) = _joint_elbow.GetRevoluteJointState().m_rValue[0] * SR_DEGREE;
#ifndef MAKE_MID_ARM_LOCK
	q(idx++) = _joint_midArm.GetRevoluteJointState().m_rValue[0] * SR_DEGREE;
#endif
	q(idx++) = _joint_wrist.GetUniversalJointState().m_rValue[0][0] * SR_DEGREE;
	q(idx++) = _joint_wrist.GetUniversalJointState().m_rValue[1][0] * SR_DEGREE;
	return q;
}

VectorXd ArmModel::getTorque()
{
	int idx = 0;
	VectorXd tau(srExt::count_dof(&_link_hand));

	tau(idx++) = _joint_shoulder.GetBallJointState().m_Torque[0];
	tau(idx++) = _joint_shoulder.GetBallJointState().m_Torque[1];
	tau(idx++) = _joint_shoulder.GetBallJointState().m_Torque[2];
	tau(idx++) = _joint_elbow.GetRevoluteJointState().m_rValue[3];
#ifndef MAKE_MID_ARM_LOCK
	tau(idx++) = _joint_midArm.GetRevoluteJointState().m_rValue[3];
#endif // !MAKE_MID_ARM_LOCK
	tau(idx++) = _joint_wrist.GetUniversalJointState().m_rValue[0][3];
	tau(idx++) = _joint_wrist.GetUniversalJointState().m_rValue[1][3];
	return tau;
}

void ArmModel::updateTension()
{
	dse3	F;
	Vec3	parent2child;

	//	reset previous external forces
	for (auto iter = _linkPtrs.begin(); iter != _linkPtrs.end(); iter++)
		iter->second->ResetUserExternalForce();

	for (auto muscle_iter = _muscles.begin(); muscle_iter != _muscles.end(); muscle_iter++)
	{
		//	apply force to parent link
		parent2child = muscle_iter->second.getPositionChild() - muscle_iter->second.getPositionParent();
		parent2child.Normalize();
		//	generalized force expressed in parent link frame
		F = dAd(SE3(-muscle_iter->second._parentPoint), dse3(Vec3(0.0), parent2child * muscle_iter->second.getTension()));
		muscle_iter->second._parentLink->m_UserExtForce += F;

		//	apply force to child link
		F = dAd(SE3(-muscle_iter->second._childPoint), dse3(Vec3(0.0), -parent2child * muscle_iter->second.getTension()));
		muscle_iter->second._childLink->m_UserExtForce += F;
	}
}

void ArmModel::assembleModel()
{
	//	define dimension of each link
	double offset = 1.0 / 100;
	double d_humerus = 6.0 / 100, l_humerus = 30.0 / 100;
	double d_ulna = 6.0 / 100, l_ulna = 25.0 / 100 / 2;
	double d_radius = 6.0 / 100, l_radius = 25.0 / 100 / 2;
	double d_hand = 5.0 / 100, w_hand = 10.0 / 100, h_hand = 12.0 / 100;

	//	base
	_link_base.GetGeomInfo().SetShape(srGeometryInfo::PLANE);
	_link_base.GetGeomInfo().SetDimension(0, 0, 0);
	_link_base.GetGeomInfo().SetColor(0, 0, 0, 1);

	//	shoulder
	_joint_shoulder.SetParentLink(&_link_base);
	_joint_shoulder.SetParentLinkFrame(SE3());
	_joint_shoulder.SetChildLink(&_link_humerus);
	_joint_shoulder.SetChildLinkFrame(EulerZYX(Vec3(0, 0, 0), Vec3(-l_humerus / 2, 0, 0)));
	_joint_shoulder.SetActType(srJoint::HYBRID);

	//	humerus
	_link_humerus.GetGeomInfo().SetShape(srGeometryInfo::BOX);
	_link_humerus.GetGeomInfo().SetDimension(l_humerus, d_humerus, d_humerus);

	//	elbow
	_joint_elbow.SetParentLink(&_link_humerus);
	_joint_elbow.SetParentLinkFrame(EulerZYX(Vec3(0, 0, -SR_PI_HALF), Vec3(l_humerus / 2, 0, 0)));
	_joint_elbow.SetChildLink(&_link_ulna);
	_joint_elbow.SetChildLinkFrame(EulerZYX(Vec3(0, 0, -SR_PI_HALF), Vec3(-l_ulna / 2, 0, 0)));
	_joint_elbow.SetActType(srJoint::HYBRID);
	_joint_elbow.MakePositionLimit(false);

	//	upper part of ulna/radius
	_link_ulna.GetGeomInfo().SetShape(srGeometryInfo::BOX);
	_link_ulna.GetGeomInfo().SetDimension(l_ulna, d_ulna, d_ulna);

	//	intermediate joint in middle of ulna/radius
	_joint_midArm.SetParentLink(&_link_ulna);
	_joint_midArm.SetParentLinkFrame(EulerZYX(Vec3(0, SR_PI_HALF, 0), Vec3(l_ulna / 2, 0, 0)));
	_joint_midArm.SetChildLink(&_link_radius);
	_joint_midArm.SetChildLinkFrame(EulerZYX(Vec3(0, SR_PI_HALF, 0), Vec3(-l_radius / 2, 0, 0)));
	_joint_midArm.SetActType(srJoint::HYBRID);
	_joint_midArm.MakePositionLimit(false);

	//	lower part of ulna/radius
	_link_radius.GetGeomInfo().SetShape(srGeometryInfo::BOX);
	_link_radius.GetGeomInfo().SetDimension(l_radius, d_radius, d_radius);

	//	wrist joint
	_joint_wrist.SetParentLink(&_link_radius);
	_joint_wrist.SetParentLinkFrame(EulerZYX(Vec3(SR_PI_HALF, 0, 0), Vec3(l_radius / 2, 0, 0)));
	_joint_wrist.SetChildLink(&_link_hand);
	_joint_wrist.SetChildLinkFrame(EulerZYX(Vec3(SR_PI_HALF, 0, 0), Vec3(-w_hand / 2, 0, 0)));
	_joint_wrist.SetActType(srJoint::HYBRID);
	_joint_wrist.MakePositionLimit(false);

	//	hand
	_link_hand.GetGeomInfo().SetShape(srGeometryInfo::BOX);
	_link_hand.GetGeomInfo().SetDimension(w_hand, d_hand, h_hand);


	SetBaseLink(&_link_base);
	SetBaseLinkType(srSystem::FIXED);
}

void ArmModel::attachMuscles()
{
	string line, tempString;
	string idx, name, l0, f0, x1, y1, z1, x2, y2, z2, link1, link2;
	ifstream file("muscle information.txt");
	stringstream lineStream;
	if (file.is_open())
		getline(file, line);

	while (file.good())
	{
		getline(file, line);
		if (line.empty())
			continue;
		lineStream = stringstream(line);
		getline(lineStream, idx, '\t');
		getline(lineStream, name, '\t');
		getline(lineStream, l0, '\t');
		getline(lineStream, f0, '\t');
		getline(lineStream, x1, '\t');
		getline(lineStream, y1, '\t');
		getline(lineStream, z1, '\t');
		getline(lineStream, x2, '\t');
		getline(lineStream, y2, '\t');
		getline(lineStream, z2, '\t');
		getline(lineStream, link1, '\t');
		getline(lineStream, link2, '\t');

		MuscleModel tempMuscle;
		tempMuscle._name = name;
		tempMuscle._parentLink = _linkPtrs[link1];
		tempMuscle._childLink = _linkPtrs[link2];

		istringstream(l0) >> tempMuscle._L0;
		istringstream(f0) >> tempMuscle._F0;
		istringstream(x1) >> tempMuscle._parentPoint[0];
		tempMuscle._parentPoint[0] /= 1000.0;
		istringstream(y1) >> tempMuscle._parentPoint[1];
		tempMuscle._parentPoint[1] /= 1000.0;
		istringstream(z1) >> tempMuscle._parentPoint[2];
		tempMuscle._parentPoint[2] /= 1000.0;
		istringstream(x2) >> tempMuscle._childPoint[0];
		tempMuscle._childPoint[0] /= 1000.0;
		istringstream(y2) >> tempMuscle._childPoint[1];
		tempMuscle._childPoint[1] /= 1000.0;
		istringstream(z2) >> tempMuscle._childPoint[2];
		tempMuscle._childPoint[2] /= 1000.0;

		if (tempMuscle._parentLink == _linkPtrs["radius"])
		{
			tempMuscle._parentPoint[0] -= (tempMuscle._parentLink->GetGeomInfo().GetDimension())[0] / 2;
			tempMuscle._parentPoint[0] -= (_linkPtrs["ulna"]->GetGeomInfo().GetDimension())[0];
		}
		else
		{
			tempMuscle._parentPoint[0] -= (tempMuscle._parentLink->GetGeomInfo().GetDimension())[0] / 2;
		}

		if (tempMuscle._childLink == _linkPtrs["radius"])
		{
			tempMuscle._childPoint[0] -= (tempMuscle._childLink->GetGeomInfo().GetDimension())[0] / 2;
			tempMuscle._childPoint[0] -= (_linkPtrs["ulna"]->GetGeomInfo().GetDimension())[0];
		}
		else
		{
			tempMuscle._childPoint[0] -= (tempMuscle._childLink->GetGeomInfo().GetDimension())[0] / 2;
		}

		_muscles.insert(make_pair(idx, tempMuscle));
	}
	file.close();
}

pair<Vector6d, VectorXd>	ArmModel::calcCounterForce(const SE3 & _counterForceFrame)
{
	Matrix6Xd	Js = srExt::Jacobian(srExt::FRAME::SPACE, &_link_hand);
	VectorXd	actuatedTorqueSum(srExt::count_dof(&_link_hand));		//	torque driven by muscle tension
	actuatedTorqueSum.setZero();
	Vector6d	fs;											//	external force of each link in spatial frame
	dse3		fs_dse3;									//	external force of each link in spatial frame (dse version)
	srLink*	currentLink;
	for (int linkIdx = 1; linkIdx < m_KIN_Links.get_size(); linkIdx++)		//	bypass base link
	{
		currentLink = m_KIN_Links[linkIdx];
		int dof = srExt::count_dof(currentLink);
		fs_dse3 = InvdAd(currentLink->GetFrame(), currentLink->m_UserExtForce);
		for (int i = 0; i < 6; i++)
			fs(i) = fs_dse3[i];
		VectorXd tau = Js.leftCols(dof).transpose() * fs;
		actuatedTorqueSum.head(tau.size()) += tau;
	}

	MatrixXd	temp = Js.transpose();
	Vector6d	counterForce = pInv(temp) * (-actuatedTorqueSum);
	//Vector6d	counterForce = temp.inverse() * (-actuatedTorqueSum);
	fs_dse3 = srExt::fromEigenVec<dse3>(counterForce);
	fs_dse3 = dAd(_counterForceFrame, fs_dse3);

	return make_pair(srExt::toEigenVec(fs_dse3, 6), temp * counterForce);
}
