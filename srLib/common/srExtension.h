#pragma once
/*!*****************************************************************************
[ SNU Robotics Extension Library ]

author		: Jisoo Hong
dependency	: Eigen 3
version		: 20161115
*******************************************************************************/
#include <ctime>
#include <Eigen/Core>
#include <Eigen/SVD>
#include <Eigen/Eigenvalues>
#include <srDyn/srSpace.h>

namespace Eigen
{
	typedef Eigen::Matrix<double, 6, 6>	Matrix6d;
	typedef Eigen::Matrix<double, 6, Eigen::Dynamic>	Matrix6Xd;
	typedef Eigen::Matrix<double, 6, 1>	Vector6d;
	
	template<typename Derived>
	MatrixXd pInv(const MatrixBase<Derived>& A)
	{
		//JacobiSVD<Derived> svd = (A.rows() < A.cols() ? Derived(A.transpose()).jacobiSvd(ComputeThinU | ComputeThinV) : A.jacobiSvd(ComputeThinU | ComputeThinV));
		//RealScalar tolerance = (RealScalar)std::numeric_limits<RealScalar>::epsilon() * std::max((RealScalar)A.cols(), (RealScalar)A.rows()) * svd.singularValues().array().abs().maxCoeff();
		//return svd.matrixV() * Derived((svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0)).asDiagonal() * svd.matrixU().adjoint();
		
		typedef typename MatrixBase<Derived>::RealScalar RealScalar;
		JacobiSVD<Derived> svd(A, ComputeThinU | ComputeThinV);
		RealScalar tolerance = std::numeric_limits<RealScalar>::epsilon() * max(A.cols(), A.rows()) *
			svd.singularValues()[0];

		auto	singVal = svd.singularValues();
		singVal = (singVal.array() > tolerance).
			select(singVal.array().inverse(), 0);
		return svd.matrixV() * singVal.asDiagonal() * svd.matrixU().adjoint();
	}
}


namespace srExt
{
	enum FRAME
	{
		SPACE,
		BODY
	};

	//	count total DOF from _joint to base or destination link (include joint it self)
	int count_dof(srJoint* _joint, srLink* const _destination = NULL);
	int count_dof(srLink* _link, srLink* const _destination = NULL);
	
	Eigen::Matrix6Xd	Jacobian(FRAME _refFrame, srLink * const _EndEffector, srLink * const _baseLink = NULL, const SE3 * const _offset = NULL);

	Eigen::Matrix6d		AdMat(const SE3& _T);

	Eigen::VectorXd		invKin(srLink* const _endEffector, const SE3& _T, srLink* _baseLink = NULL, double _tol = 1e-7);

	void				setJointValue(srLink* const _endEffector, const Eigen::VectorXd& _q);
	void				addJointValue(srLink* const _endEffector, const Eigen::VectorXd& _dq);
	Eigen::VectorXd		getJointValue(srLink* const _endEffector, srLink* _baseLink = NULL);
	void				adjust2Limit(srJoint* const _joint);

	void				avoidPosLimit(srJoint* const _joint);
	void				avoidPosLimit(srLink* const _endEffector, srLink* _baseLink = NULL);


	template<class SRtype>
	Eigen::VectorXd toEigenVec(const SRtype& _vec, int _numel)
	{
		Eigen::VectorXd ret(_numel);
		for (int i = 0; i < _numel; i++)
			ret(i) = _vec[i];
		return ret;
	}

	template<class SRtype, class MatrixType>
	SRtype fromEigenVec(const MatrixType& _vec)
	{
		SRtype	ret;
		for (int i = 0; i < _vec.size(); i++)
			ret[i] = _vec(i);
		return ret;
	}
}