#include "srExtension.h"
using namespace srExt;
using namespace Eigen;

int srExt::count_dof(srJoint * _joint, srLink * const _destination)
{
	int dof = 0;
	srJoint* currentJoint = _joint;
	while (true)
	{
		switch (currentJoint->GetType())
		{
		case srJoint::JOINTTYPE::REVOLUTE:
		case srJoint::JOINTTYPE::PRISMATIC:
			dof += 1;
			break;
		case srJoint::JOINTTYPE::UNIVERSAL:
			dof += 2;
			break;
		case srJoint::JOINTTYPE::BALL:
			dof += 3;
			break;
		case srJoint::JOINTTYPE::WELD:
			;	// 0 DOF
		default:
			//	invalid joint type!!
			break;
		}

		if (_destination == NULL && currentJoint->m_ParentLink->m_IsBaseLink)
			break;
		else if
			(_destination != NULL && currentJoint->m_ParentLink == _destination)
			break;
		else
			currentJoint = currentJoint->m_ParentLink->m_ParentJoint;
	}

	return dof;
}

int srExt::count_dof(srLink * _link, srLink * const _destination)
{
	if (_link->m_IsBaseLink)
		return 0;
	else
		return count_dof(_link->m_ParentJoint, _destination);
}

Eigen::Matrix6Xd srExt::Jacobian(FRAME _refFrame, srLink * const _EndEffector, srLink * const _baseLink, const SE3 * const _offset)
{
	Eigen::Matrix6Xd J(6, srExt::count_dof(_EndEffector, _baseLink));	//	6 by n space Jacobian matrix
	srLink * childLink = _EndEffector;			//	
	srJoint * currentJoint = NULL;

	SE3	ref2childLink;		//	frame of child link in reference frame ( body : end effector , spatial : fixed frame )
	se3 temp_se3;
	se3& screw_local = temp_se3;		//	screw axis of joint in child link frame
	se3 screw_global;		//	screw axis of joint in space frame

	int colIdx = 0;
	while (!childLink->m_IsBaseLink)
	{
		currentJoint = childLink->m_ParentJoint;
		if (_refFrame == srExt::FRAME::BODY && _offset == NULL)
			ref2childLink = _EndEffector->GetFrame() % childLink->GetFrame();
		else if (_refFrame == srExt::FRAME::BODY && _offset != NULL)
			ref2childLink = (_EndEffector->GetFrame() * *_offset) % childLink->GetFrame();
		else if (_refFrame == srExt::FRAME::SPACE)
			ref2childLink = childLink->GetFrame();

		switch (currentJoint->GetType())
		{
		case srJoint::JOINTTYPE::REVOLUTE:
			screw_local = static_cast<srRevoluteJoint*>(currentJoint)->m_FS_Screw;
			screw_global = Ad(ref2childLink, screw_local);
			for (int i = 0; i < 6; i++)
				J(i, J.cols() - colIdx - 1) = screw_global[i];
			colIdx++;
			break;

		case srJoint::JOINTTYPE::PRISMATIC:
			screw_local = static_cast<srPrismaticJoint*>(currentJoint)->m_FS_Screw;
			screw_global = Ad(ref2childLink, screw_local);
			for (int i = 0; i < 6; i++)
				J(i, J.cols() - colIdx - 1) = screw_global[i];
			colIdx++;
			break;

		case srJoint::JOINTTYPE::UNIVERSAL:
			screw_local = static_cast<srUniversalJoint*>(currentJoint)->m_FS_Screw2;
			screw_global = Ad(ref2childLink, screw_local);
			for (int i = 0; i < 6; i++)
				J(i, J.cols() - colIdx - 1) = screw_global[i];
			colIdx++;

			screw_local = static_cast<srUniversalJoint*>(currentJoint)->m_FS_Screw1;
			screw_global = Ad(ref2childLink, screw_local);
			for (int i = 0; i < 6; i++)
				J(i, J.cols() - colIdx - 1) = screw_global[i];
			colIdx++;
			break;

		case srJoint::JOINTTYPE::BALL:
			for (int j = 2; j >= 0; j--)
			{
				screw_local = static_cast<srBallJoint*>(currentJoint)->m_FS_Screw[j];
				screw_global = Ad(ref2childLink, screw_local);
				for (int i = 0; i < 6; i++)
					J(i, J.cols() - colIdx - 1) = screw_global[i];
				colIdx++;
			}
			break;

		default:
			break;
		}
		childLink = childLink->m_ParentLink;
	}

	return J;
}

Eigen::Matrix6d srExt::AdMat(const SE3 & _T)
{
	Eigen::Matrix6d AdMatrix = Eigen::Matrix6d::Zero();

	//	fill upper left and lower right block
	for (int row = 0; row < 3; row++)
		for (int col = 0; col < 3; col++)
		{
			AdMatrix(row, col) = _T(row, col);
			AdMatrix(row + 3, col + 3) = _T(row, col);
		}

	//	fill lower left block
	double p0 = _T(0, 3);
	double p1 = _T(1, 3);
	double p2 = _T(2, 3);
	Eigen::Matrix3d	p_skew;
	p_skew <<
		0, -p2, p1,
		p2, 0, -p0,
		-p1, p0, 0;

	AdMatrix.block(3, 0, 3, 3) = p_skew * AdMatrix.block(0, 0, 3, 3);

	return AdMatrix;
}

Eigen::VectorXd srExt::invKin(srLink * const _endEffector, const SE3& _T, srLink * _baseLink, double _tol)
{
	//	0 : fixed frame
	//	E : end-effector frame
	//	R : reference frame
	//	G : goal-frame
	//	_T : T_RG
	SE3		goalSE3_EE;								//	T_EG
	se3		currError;								//	se3 error seen by Global frame
	double	errorNorm = 1e+10;						//	Norm of se3 error

	int _iter = 0;
	const unsigned int _max_iter = 100;
	Eigen::VectorXd	dX(6), dQ, initialGuess;

	if (_baseLink == NULL)
		_baseLink = _endEffector->m_pSystem->GetBaseLink();

	//_endEffector->m_pSystem->BackupInitState();
	for (int trial = 0; trial < 10; trial++)
	{
		errorNorm = 1e+17;
		_iter = 0;

		if (trial == 2)
		{
			initialGuess = VectorXd::Zero(count_dof(_endEffector, _baseLink));
			srExt::setJointValue(_endEffector, initialGuess);
		}
		else if (trial > 2)
		{
			std::srand((unsigned int)std::time(0));
			initialGuess = VectorXd::Random(count_dof(_endEffector, _baseLink));
			srExt::setJointValue(_endEffector, initialGuess);
		}

		while (errorNorm > _tol)
		{
			if (_iter++ > _max_iter)
				break;
			_endEffector->m_pSystem->KIN_UpdateFrame_All_The_Entity();
			goalSE3_EE = _endEffector->GetFrame() % (_baseLink->GetFrame() * _T);	//	T_EG = T_E0 * T_0R * T_RG
			currError = Log(goalSE3_EE);					//	se3 error seen by EE frame
			errorNorm = sqrt(SquareSum(currError));
			for (int i = 0; i < 6; i++)
				dX[i] = currError[i];
			//dQ = Eigen::pInv(srExt::Jacobian(srExt::FRAME::BODY, _endEffector, _baseLink)) * dX;
			dQ = srExt::Jacobian(srExt::FRAME::BODY, _endEffector, _baseLink).jacobiSvd(ComputeThinU | ComputeThinV).solve(dX);
			addJointValue(_endEffector, dQ);
			avoidPosLimit(_endEffector, _baseLink);
		}
		if (errorNorm < _tol)
			return getJointValue(_endEffector, _baseLink);
	}
	
	cout << "Kibo::invKinematics failure (exceed maximum iterations)." << endl;
	return VectorXd::Zero(count_dof(_endEffector, _baseLink));
}

void srExt::setJointValue(srLink * const _endEffector, const VectorXd & _q)
{
	int idx = _q.size() - 1;
	srLink* childLink = _endEffector;
	srJoint* currentJoint;

	while (!childLink->m_IsBaseLink)
	{
		currentJoint = childLink->m_ParentJoint;

		switch (currentJoint->GetType())
		{
		case srJoint::JOINTTYPE::REVOLUTE:
			static_cast<srRevoluteJoint*>(currentJoint)->GetRevoluteJointState().m_rValue[0] = _q[idx];
			idx--;
			break;

		case srJoint::JOINTTYPE::PRISMATIC:
			static_cast<srPrismaticJoint*>(currentJoint)->GetPrismaticJointState().m_rValue[0] = _q[idx];
			idx--;
			break;

		case srJoint::JOINTTYPE::UNIVERSAL:
			static_cast<srUniversalJoint*>(currentJoint)->GetUniversalJointState().m_rValue[1][0] = _q[idx];
			static_cast<srUniversalJoint*>(currentJoint)->GetUniversalJointState().m_rValue[0][0] = _q[idx - 1];
			idx -= 2;
			break;

		case srJoint::JOINTTYPE::BALL:
			static_cast<srBallJoint*>(currentJoint)->GetBallJointState().m_SO3Pos = EulerZYX(Vec3(_q[idx], _q[idx - 1], _q[idx - 2])).GetOrientation();
			idx -= 3;
			break;
		default:
			break;
		}
		//adjust2Limit(currentJoint);

		if (idx < 0)
			break;
		childLink = childLink->m_ParentLink;
	}
}

void srExt::addJointValue(srLink * const _endEffector, const VectorXd & _dq)
{
	int idx = _dq.size() - 1;
	srLink* childLink = _endEffector;
	srJoint* currentJoint;
	Vec3	ballJointState;
	while (!childLink->m_IsBaseLink)
	{
		currentJoint = childLink->m_ParentJoint;

		switch (currentJoint->GetType())
		{
		case srJoint::JOINTTYPE::REVOLUTE:
			static_cast<srRevoluteJoint*>(currentJoint)->GetRevoluteJointState().m_rValue[0] += _dq[idx];
			idx--;
			break;

		case srJoint::JOINTTYPE::PRISMATIC:
			static_cast<srPrismaticJoint*>(currentJoint)->GetPrismaticJointState().m_rValue[0] += _dq[idx];
			idx--;
			break;

		case srJoint::JOINTTYPE::UNIVERSAL:
			static_cast<srUniversalJoint*>(currentJoint)->GetUniversalJointState().m_rValue[1][0] += _dq[idx];
			static_cast<srUniversalJoint*>(currentJoint)->GetUniversalJointState().m_rValue[0][0] += _dq[idx - 1];
			idx -= 2;
			break;

		case srJoint::JOINTTYPE::BALL:
			ballJointState = iEulerZYX(static_cast<srBallJoint*>(currentJoint)->GetBallJointState().m_SO3Pos);
			static_cast<srBallJoint*>(currentJoint)->GetBallJointState().m_SO3Pos = EulerZYX(Vec3(_dq[idx], _dq[idx - 1], _dq[idx - 2]) + ballJointState).GetOrientation();
			idx -= 3;
			break;
		default:
			break;
		}
		//adjust2Limit(currentJoint);
		if (idx < 0)
			break;
		childLink = childLink->m_ParentLink;
	}
}

Eigen::VectorXd srExt::getJointValue(srLink * const _endEffector, srLink * _baseLink)
{
	if (_baseLink == NULL)
		_baseLink = _endEffector->m_pSystem->GetBaseLink();
	VectorXd q(count_dof(_endEffector, _baseLink));
	int idx = q.size() - 1;
	srLink* childLink = _endEffector;
	srJoint* currentJoint;
	Vec3	ballJointState;
	while (childLink != _baseLink)
	{
		currentJoint = childLink->m_ParentJoint;

		switch (currentJoint->GetType())
		{
		case srJoint::JOINTTYPE::REVOLUTE:
			q[idx] = static_cast<srRevoluteJoint*>(currentJoint)->GetRevoluteJointState().m_rValue[0];
			idx--;
			break;

		case srJoint::JOINTTYPE::PRISMATIC:
			q[idx] = static_cast<srPrismaticJoint*>(currentJoint)->GetPrismaticJointState().m_rValue[0];
			idx--;
			break;

		case srJoint::JOINTTYPE::UNIVERSAL:
			q[idx] = static_cast<srUniversalJoint*>(currentJoint)->GetUniversalJointState().m_rValue[1][0];
			q[idx - 1] = static_cast<srUniversalJoint*>(currentJoint)->GetUniversalJointState().m_rValue[0][0];
			idx -= 2;
			break;

		case srJoint::JOINTTYPE::BALL:
			ballJointState = iEulerZYX(static_cast<srBallJoint*>(currentJoint)->GetBallJointState().m_SO3Pos);
			q[idx] = ballJointState[0];
			q[idx - 1] = ballJointState[1];
			q[idx - 2] = ballJointState[2];
			idx -= 3;
			break;
		case srJoint::JOINTTYPE::WELD:
		default:
			break;
		}
		childLink = childLink->m_ParentLink;
	}
	return q;
}

void srExt::adjust2Limit(srJoint * const _joint)
{
	switch (_joint->GetType())
	{
	case srJoint::JOINTTYPE::REVOLUTE:
		if (static_cast<srRevoluteJoint*>(_joint)->IsPostionLimited())
		{
			if (static_cast<srRevoluteJoint*>(_joint)->GetRevoluteJointState().m_rValue[0] > static_cast<srRevoluteJoint*>(_joint)->GetPositionUpperLimit()*SR_RADIAN)
				static_cast<srRevoluteJoint*>(_joint)->GetRevoluteJointState().m_rValue[0] = static_cast<srRevoluteJoint*>(_joint)->GetPositionUpperLimit()*SR_RADIAN;
			if (static_cast<srRevoluteJoint*>(_joint)->GetRevoluteJointState().m_rValue[0] < static_cast<srRevoluteJoint*>(_joint)->GetPositionLowerLimit()*SR_RADIAN)
				static_cast<srRevoluteJoint*>(_joint)->GetRevoluteJointState().m_rValue[0] = static_cast<srRevoluteJoint*>(_joint)->GetPositionLowerLimit()*SR_RADIAN;
		}
		break;
	case srJoint::JOINTTYPE::PRISMATIC:
		if (static_cast<srPrismaticJoint*>(_joint)->IsPostionLimited())
		{
			if (static_cast<srPrismaticJoint*>(_joint)->GetPrismaticJointState().m_rValue[0] > static_cast<srPrismaticJoint*>(_joint)->GetPositionUpperLimit())
				static_cast<srPrismaticJoint*>(_joint)->GetPrismaticJointState().m_rValue[0] = static_cast<srPrismaticJoint*>(_joint)->GetPositionUpperLimit();
			if (static_cast<srPrismaticJoint*>(_joint)->GetPrismaticJointState().m_rValue[0] < static_cast<srPrismaticJoint*>(_joint)->GetPositionLowerLimit())
				static_cast<srPrismaticJoint*>(_joint)->GetPrismaticJointState().m_rValue[0] = static_cast<srPrismaticJoint*>(_joint)->GetPositionLowerLimit();
		}
		break;
	case srJoint::JOINTTYPE::UNIVERSAL:
		for (int i = 0; i < 2; i++)
		{
			if (static_cast<srUniversalJoint*>(_joint)->IsPostionLimited(i))
			{
				if (static_cast<srUniversalJoint*>(_joint)->GetUniversalJointState().m_rValue[i][0] > static_cast<srUniversalJoint*>(_joint)->GetPositionUpperLimit(i)*SR_RADIAN)
					static_cast<srUniversalJoint*>(_joint)->GetUniversalJointState().m_rValue[i][0] = static_cast<srUniversalJoint*>(_joint)->GetPositionUpperLimit(i)*SR_RADIAN;
				if (static_cast<srUniversalJoint*>(_joint)->GetUniversalJointState().m_rValue[i][0] < static_cast<srUniversalJoint*>(_joint)->GetPositionLowerLimit(i)*SR_RADIAN)
					static_cast<srUniversalJoint*>(_joint)->GetUniversalJointState().m_rValue[i][0] = static_cast<srUniversalJoint*>(_joint)->GetPositionLowerLimit(i)*SR_RADIAN;
			}
		}
		break;
	case srJoint::JOINTTYPE::BALL:
		break;
	default:
		break;
	}
}

void srExt::avoidPosLimit(srJoint * const _joint)
{
	switch (_joint->GetType())
	{
	case srJoint::JOINTTYPE::REVOLUTE:
		if (static_cast<srRevoluteJoint*>(_joint)->IsPostionLimited() )
		{
			if (static_cast<srRevoluteJoint*>(_joint)->GetRevoluteJointState().m_rValue[0] > static_cast<srRevoluteJoint*>(_joint)->GetPositionUpperLimit()*SR_RADIAN ||
				static_cast<srRevoluteJoint*>(_joint)->GetRevoluteJointState().m_rValue[0] < static_cast<srRevoluteJoint*>(_joint)->GetPositionLowerLimit()*SR_RADIAN)
			{
				double m = (static_cast<srRevoluteJoint*>(_joint)->GetPositionUpperLimit() + static_cast<srRevoluteJoint*>(_joint)->GetPositionLowerLimit())*SR_RADIAN / 2;
				static_cast<srRevoluteJoint*>(_joint)->GetRevoluteJointState().m_rValue[0] -= m;
				static_cast<srRevoluteJoint*>(_joint)->GetRevoluteJointState().m_rValue[0] *= -0.5;
				static_cast<srRevoluteJoint*>(_joint)->GetRevoluteJointState().m_rValue[0] += m;
			}
		}
		break;
	case srJoint::JOINTTYPE::PRISMATIC:
		if (static_cast<srPrismaticJoint*>(_joint)->IsPostionLimited())
		{
			if (static_cast<srPrismaticJoint*>(_joint)->GetPrismaticJointState().m_rValue[0] > static_cast<srPrismaticJoint*>(_joint)->GetPositionUpperLimit() ||
				static_cast<srPrismaticJoint*>(_joint)->GetPrismaticJointState().m_rValue[0] < static_cast<srPrismaticJoint*>(_joint)->GetPositionLowerLimit())
			{
				double m = (static_cast<srPrismaticJoint*>(_joint)->GetPositionUpperLimit() + static_cast<srPrismaticJoint*>(_joint)->GetPositionLowerLimit()) / 2;
				static_cast<srPrismaticJoint*>(_joint)->GetPrismaticJointState().m_rValue[0] -= m;
				static_cast<srPrismaticJoint*>(_joint)->GetPrismaticJointState().m_rValue[0] *= -0.5;
				static_cast<srPrismaticJoint*>(_joint)->GetPrismaticJointState().m_rValue[0] += m;
			}
		}
		break;
	case srJoint::JOINTTYPE::UNIVERSAL:
		for (int i = 0; i < 2; i++)
		{
			if (static_cast<srUniversalJoint*>(_joint)->IsPostionLimited(i))
			{
				if (static_cast<srUniversalJoint*>(_joint)->GetUniversalJointState().m_rValue[i][0] > static_cast<srUniversalJoint*>(_joint)->GetPositionUpperLimit(i)*SR_RADIAN ||
					static_cast<srUniversalJoint*>(_joint)->GetUniversalJointState().m_rValue[i][0] < static_cast<srUniversalJoint*>(_joint)->GetPositionLowerLimit(i)*SR_RADIAN)
				{
					double m = (static_cast<srUniversalJoint*>(_joint)->GetPositionUpperLimit(i) + static_cast<srUniversalJoint*>(_joint)->GetPositionLowerLimit(i))*SR_RADIAN / 2;
					static_cast<srUniversalJoint*>(_joint)->GetUniversalJointState().m_rValue[i][0] -= m;
					static_cast<srUniversalJoint*>(_joint)->GetUniversalJointState().m_rValue[i][0] *= -0.5;
					static_cast<srUniversalJoint*>(_joint)->GetUniversalJointState().m_rValue[i][0] += m;
				}
			}
		}
		break;
	case srJoint::JOINTTYPE::BALL:
		break;
	default:
		break;
	}
}

void srExt::avoidPosLimit(srLink * const _endEffector, srLink * _baseLink)
{
	if (_baseLink == NULL)
		_baseLink = _endEffector->m_pSystem->GetBaseLink();
	srLink* childLink = _endEffector;

	while (childLink != _baseLink)
	{
		avoidPosLimit(childLink->m_ParentJoint);
		childLink = childLink->m_ParentLink;
	}
}
